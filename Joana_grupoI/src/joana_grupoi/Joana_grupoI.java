/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joana_grupoi;

import java.util.Scanner;

/**
 *
 * @author tesp1b
 */
public class Joana_grupoI {

    /**
     * @param args the command line arguments
     * 
     * variavel nVisitantes numero de visitantes
     * variavel somaVisitantes soma o total de visitantes ao longo da duração da viagem medieval
     */
    public static void main(String[] args) {
        int nVisitantes=0, somaVisitantes=0, visitanteMaior=0, diaMaior = 1, diaMenor = 1, visitanteMenor=9999,primeiroDia, ultimoDia;
        float media=0, primeiroDiaPercent, ultimoDiaPercent;
        Scanner ler = new Scanner (System.in);
                
        for (int i =1; i<15; i++){
            System.out.println("Introduza o numero de visitantes do " + i + "º dia");
            nVisitantes = ler.nextInt();
            somaVisitantes += nVisitantes;
                if (visitanteMaior<nVisitantes) {
                    visitanteMaior=nVisitantes;
                    diaMaior = i;
                }
                if (visitanteMenor>nVisitantes) {
                    visitanteMenor=nVisitantes;
                    diaMenor = i;
                }
                if (i==1) {
                    primeiroDia=nVisitantes;
                }
                if (i==14) {
                    ultimoDia=nVisitantes;
                }    
        }
        
        media = (float)somaVisitantes/14;
        primeiroDiaPercent=(float)primeiroDia/somaVisitantes*100;
        ultimoDiaPercent=(float)ultimoDia/somaVisitantes*100;
        System.out.println("A média de visitantes diária é " + media);
        System.out.println("O dia com maior numero de visitantes é no dia " + diaMaior + " com " + visitanteMaior + " visitante." );
        System.out.println("O dia com menor numero de visitantes é no dia " + diaMenor + " com " + visitanteMenor + " visitante." );
        }
}
